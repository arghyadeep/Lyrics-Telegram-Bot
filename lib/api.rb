require 'uri'
require 'net/http'
require 'json'
require 'openssl'

require_relative 'bot.rb'

class Api
    def getTrackId (track_name)
        url = URI("https://shazam.p.rapidapi.com/search?locale=en-US&offset=0&limit=5&term=#{track_name}")

        http = Net::HTTP.new(url.host, url.port)
        http.use_ssl = true
        http.verify_mode = OpenSSL::SSL::VERIFY_NONE

        request = Net::HTTP::Get.new(url)
        request["x-rapidapi-host"] = 'shazam.p.rapidapi.com'
        request["x-rapidapi-key"] = 'c24fddcb0dmshce98ed4625013b2p1ecf75jsn55b4e325c38d'

        response = http.request(request)
        response = JSON.parse(response)

        track_id = response.tracks.hits[0].track.key
    
        track_id
    end

    def getLyrics (track_id)
        url = URI("https://shazam.p.rapidapi.com/songs/get-details?locale=en-US&key=#{track_id}")
        http = Net::HTTP.new(url.host, url.port)
        http.use_ssl = true
        http.verify_mode = OpenSSL::SSL::VERIFY_NONE

        request = Net::HTTP::Get.new(url)
        request["x-rapidapi-host"] = 'shazam.p.rapidapi.com'
        request["x-rapidapi-key"] = 'c24fddcb0dmshce98ed4625013b2p1ecf75jsn55b4e325c38d'

        response = http.request(request)
        response = JSON.parse(response)

        #lyrics = response.sections.type[]

        lyricsArr = response["sections"].select {|h1| h1['type']=='LYRICS'}.first['text']

        lyrics = ""

        lyricsArr.each do |item|
            lyrics = lyrics + item + "\n"
        end

        lyrics
    end

    def call (track_name)
        trackId = getTrackId(track_name)
        lyrics = getLyrics(track_id)

        lyrics
    end
end