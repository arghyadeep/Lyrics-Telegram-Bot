require 'telegram/bot'

require_relative 'api.rb'           

class Bot
    def initialize
        token = ''
        
    Telegram::Bot::Client.run(token) do |bot|
        bot.listen do |message|
            case message.text

            when '/help'
                bot.api.send_message(chat_id: message.chat.id, text: "Hello, #{message.from.first_name}!\nWelcome to Lyricist. Just text me a song name and I'll get the entire lyrics for you. ;-)")
            end
        end
    end
    end
end
